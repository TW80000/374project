LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE STD.textio.ALL;
USE IEEE.std_logic_textio.ALL;

ENTITY mux16to1 IS
    PORT 
    (
        IN0, IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12, IN13, IN14, IN15  : IN std_logic_vector(31 DOWNTO 0);
        SEL       : IN std_logic_vector(3 DOWNTO 0);
        MUX_OUT   : OUT std_logic_vector (31 DOWNTO 0)
    );
END mux16to1;

ARCHITECTURE Behavioural OF mux16to1 IS
BEGIN
	MUX_OUT <= IN0 WHEN (SEL = "0000") ELSE
				  IN1 WHEN (SEL = "0001") ELSE
				  IN2 WHEN (SEL = "0010") ELSE
				  IN3 WHEN (SEL = "0011") ELSE
				  IN4 WHEN (SEL = "0100") ELSE
				  IN5 WHEN (SEL = "0101") ELSE
				  IN6 WHEN (SEL = "0110") ELSE
				  IN7 WHEN (SEL = "0111") ELSE
				  IN8 WHEN (SEL = "1000") ELSE
				  IN9 WHEN (SEL = "1001") ELSE
				  IN10 WHEN (SEL = "1010") ELSE
				  IN11 WHEN (SEL = "1011") ELSE
				  IN12 WHEN (SEL = "1100") ELSE
				  IN13 WHEN (SEL = "1101") ELSE
				  IN14 WHEN (SEL = "1110") ELSE
				  IN15 WHEN (SEL = "1111");
END Behavioural;