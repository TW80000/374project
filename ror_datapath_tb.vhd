-- ror_datapath_tb.vhd file: <This is the filename>
LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- entity declaration only; no definition here
ENTITY ror_datapath_tb IS
END;

-- Architecture of the testbench with the signal names
ARCHITECTURE ror_datapath_tb_arch OF ror_datapath_tb IS

	-- Add any other signal you would like to see in your simulation
	SIGNAL INC_PC_tb, R0_drive_tb, R1_drive_tb, R2_drive_tb, R3_drive_tb,
	R4_drive_tb, R5_drive_tb, R6_drive_tb, R7_drive_tb, R8_drive_tb, 
	R9_drive_tb, R10_drive_tb, R11_drive_tb, R12_drive_tb, R13_drive_tb, 
	R14_drive_tb, R15_drive_tb, HI_drive_tb, LO_drive_tb, Zhigh_drive_tb, 
	Zlow_drive_tb, PC_drive_tb, MDR_drive_tb, R0_en_inpin_tb, R1_en_inpin_tb, 
	R2_en_inpin_tb, R3_en_inpin_tb, R4_en_inpin_tb, R5_en_inpin_tb, 
	R6_en_inpin_tb, R7_en_inpin_tb, R8_en_inpin_tb, R9_en_inpin_tb, 
	R10_en_inpin_tb, R11_en_inpin_tb, R12_en_inpin_tb, R13_en_inpin_tb, 
	R14_en_inpin_tb, R15_en_inpin_tb, HI_en_inpin_tb, LO_en_inpin_tb, 
	PC_en_inpin_tb, IR_en_inpin_tb, Zhigh_en_inpin_tb, Zlow_en_inpin_tb, 
	Y_en_inpin_tb, MDR_en_inpin_tb, read_mem_inpin_tb, MAR_en_tb, 
	Clear_tb, Clock_tb, ADD_OP_tb, SUB_OP_tb, MUL_OP_tb, DIV_OP_tb,
	SHR_OP_tb, SHL_OP_tb, ROR_OP_tb, ROL_OP_tb, AND_OP_tb, OR_OP_tb,
	NEG_OP_tb, NOT_OP_tb: STD_LOGIC;
	SIGNAL Mdatain_tb, BusMuxOut_tb, MDR_tb, R1_tb, R2_tb, R3_tb, Zlow_tb : STD_LOGIC_VECTOR(31 DOWNTO 0);
	TYPE State IS (default, Reg_load1, Reg_load2, Reg_load3, Reg_load4, T0, T1, T2, T3, T4, T5);
	SIGNAL Present_state: State := default;

	-- component instantiation of the datapath
	COMPONENT datapath
	PORT (
		INC_PC :  IN  STD_LOGIC;
		R0_drive :  IN  STD_LOGIC;
		R1_drive :  IN  STD_LOGIC;
		R2_drive :  IN  STD_LOGIC;
		R3_drive :  IN  STD_LOGIC;
		R4_drive :  IN  STD_LOGIC;
		R5_drive :  IN  STD_LOGIC;
		R6_drive :  IN  STD_LOGIC;
		R7_drive :  IN  STD_LOGIC;
		R8_drive :  IN  STD_LOGIC;
		R9_drive :  IN  STD_LOGIC;
		R10_drive :  IN  STD_LOGIC;
		R11_drive :  IN  STD_LOGIC;
		R12_drive :  IN  STD_LOGIC;
		R13_drive :  IN  STD_LOGIC;
		R14_drive :  IN  STD_LOGIC;
		R15_drive :  IN  STD_LOGIC;
		HI_drive :  IN  STD_LOGIC;
		LO_drive :  IN  STD_LOGIC;
		Zhigh_drive :  IN  STD_LOGIC;
		Zlow_drive :  IN  STD_LOGIC;
		PC_drive :  IN  STD_LOGIC;
		MDR_drive :  IN  STD_LOGIC;
		R0_en_inpin :  IN  STD_LOGIC;
		R1_en_inpin :  IN  STD_LOGIC;
		R2_en_inpin :  IN  STD_LOGIC;
		R3_en_inpin :  IN  STD_LOGIC;
		R4_en_inpin :  IN  STD_LOGIC;
		R5_en_inpin :  IN  STD_LOGIC;
		R6_en_inpin :  IN  STD_LOGIC;
		R7_en_inpin :  IN  STD_LOGIC;
		R8_en_inpin :  IN  STD_LOGIC;
		R9_en_inpin :  IN  STD_LOGIC;
		R10_en_inpin :  IN  STD_LOGIC;
		R11_en_inpin :  IN  STD_LOGIC;
		R12_en_inpin :  IN  STD_LOGIC;
		R13_en_inpin :  IN  STD_LOGIC;
		R14_en_inpin :  IN  STD_LOGIC;
		R15_en_inpin :  IN  STD_LOGIC;
		HI_en_inpin :  IN  STD_LOGIC;
		LO_en_inpin :  IN  STD_LOGIC;
		PC_en_inpin :  IN  STD_LOGIC;
		IR_en_inpin :  IN  STD_LOGIC;
		Zhigh_en_inpin :  IN  STD_LOGIC;
		Zlow_en_inpin :  IN  STD_LOGIC;
		Y_en_inpin :  IN  STD_LOGIC;
		MDR_en_inpin :  IN  STD_LOGIC;
		read_mem_inpin :  IN  STD_LOGIC;
		MAR_en :  IN  STD_LOGIC;
		ADD_OP :  IN  STD_LOGIC;
		SUB_OP :  IN  STD_LOGIC;
		MUL_OP :  IN  STD_LOGIC;
		DIV_OP :  IN  STD_LOGIC;
		AND_OP :  IN  STD_LOGIC;
		OR_OP  :  IN  STD_LOGIC;
		SHR_OP :  IN  STD_LOGIC;
		SHL_OP :  IN  STD_LOGIC;
		ROR_OP :  IN  STD_LOGIC;
		ROL_OP :  IN  STD_LOGIC;
		NEG_OP :  IN  STD_LOGIC;
		NOT_OP :  IN  STD_LOGIC;
		Clear  :  IN  STD_LOGIC;
		Clock  :  IN  STD_LOGIC;
		Mdatain :  IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		BusMuxOut :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0);
		MDR :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0);
		R1 :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0);
		R2 :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0);
		R3 :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0);
		Zlow :  OUT  STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
	END COMPONENT datapath;
BEGIN
	DUT : datapath
	--port mapping: between the dut and the testbench signals
	PORT MAP (
		INC_PC => INC_PC_tb,
		R0_drive => R0_drive_tb,
		R1_drive => R1_drive_tb,
		R2_drive => R2_drive_tb,
		R3_drive => R3_drive_tb,
		R4_drive => R4_drive_tb,
		R5_drive => R5_drive_tb,
		R6_drive => R6_drive_tb,
		R7_drive => R7_drive_tb,
		R8_drive => R8_drive_tb,
		R9_drive => R9_drive_tb,
		R10_drive => R10_drive_tb,
		R11_drive => R11_drive_tb,
		R12_drive => R12_drive_tb,
		R13_drive => R13_drive_tb,
		R14_drive => R14_drive_tb,
		R15_drive => R15_drive_tb,
		HI_drive => HI_drive_tb,
		LO_drive => LO_drive_tb,
		Zhigh_drive => Zhigh_drive_tb,
		Zlow_drive => Zlow_drive_tb,
		PC_drive => PC_drive_tb,
		MDR_drive => MDR_drive_tb,
		R0_en_inpin => R0_en_inpin_tb,
		R1_en_inpin => R1_en_inpin_tb,
		R2_en_inpin => R2_en_inpin_tb,
		R3_en_inpin => R3_en_inpin_tb,
		R4_en_inpin => R4_en_inpin_tb,
		R5_en_inpin => R5_en_inpin_tb,
		R6_en_inpin => R6_en_inpin_tb,
		R7_en_inpin => R7_en_inpin_tb,
		R8_en_inpin => R8_en_inpin_tb,
		R9_en_inpin => R9_en_inpin_tb,
		R10_en_inpin => R10_en_inpin_tb,
		R11_en_inpin => R11_en_inpin_tb,
		R12_en_inpin => R12_en_inpin_tb,
		R13_en_inpin => R13_en_inpin_tb,
		R14_en_inpin => R14_en_inpin_tb,
		R15_en_inpin => R15_en_inpin_tb,
		HI_en_inpin => HI_en_inpin_tb,
		LO_en_inpin => LO_en_inpin_tb,
		PC_en_inpin => PC_en_inpin_tb,
		IR_en_inpin => IR_en_inpin_tb,
		Zhigh_en_inpin => Zhigh_en_inpin_tb,
		Zlow_en_inpin => Zlow_en_inpin_tb,
		Y_en_inpin => Y_en_inpin_tb,
		MDR_en_inpin => MDR_en_inpin_tb,
		read_mem_inpin => read_mem_inpin_tb,
		MAR_en => MAR_en_tb,
		ADD_OP => ADD_OP_tb,
		SUB_OP => SUB_OP_tb,
		MUL_OP => MUL_OP_tb,
		DIV_OP => DIV_OP_tb,
		SHL_OP => SHL_OP_tb,
		SHR_OP => SHR_OP_tb,
		AND_OP => AND_OP_tb,
		OR_OP => OR_OP_tb,
		ROR_OP => ROR_OP_tb,
		ROL_OP => ROL_OP_tb,
		NEG_OP => NEG_OP_tb,
		NOT_OP => NOT_OP_tb,
		Clear => Clear_tb,
		Clock => Clock_tb,
		Mdatain => Mdatain_tb,
		BusMuxOut => BusMuxOut_tb,
		MDR => MDR_tb,
		R1 => R1_tb,
		R2 => R2_tb,
		R3 => R3_tb,
		Zlow => Zlow_tb
	);
	
	--add test logic here
	Clock_process: PROCESS
	BEGIN
		Clock_tb <= '1', '0' after 20 ns;
		Wait for 40 ns;
	END PROCESS Clock_process;
	
	PROCESS(Clock_tb) -- finite state machine
	BEGIN
		IF (Clock_tb'EVENT AND Clock_tb = '1') THEN -- if clock rising-edge
			CASE Present_state IS
				WHEN Default =>
					Present_state <= Reg_load1;
				WHEN Reg_load1 =>
					Present_state <= Reg_load2;
				WHEN Reg_load2 =>
					Present_state <= Reg_load3;
				WHEN Reg_load3 =>
					Present_state <= Reg_load4;
				WHEN Reg_load4 =>
					Present_state <= T0;
				WHEN T0 =>
					Present_state <= T1;
				WHEN T1 =>
					Present_state <= T2;
				WHEN T2 =>
					Present_state <= T3;
				WHEN T3 =>
					Present_state <= T4;
				WHEN T4 =>
					Present_state <= T5;
				WHEN OTHERS =>
			END CASE;
		END IF;
	END PROCESS;
	
	PROCESS(Present_state) -- do the required job in each state
	BEGIN
		CASE Present_state IS -- assert the required signals in each clock cycle
			WHEN Default =>
				INC_PC_tb <= '0';
				R0_drive_tb <= '0';
				R1_drive_tb <= '0';
				R2_drive_tb <= '0';
				R3_drive_tb <= '0';
				R4_drive_tb <= '0';
				R5_drive_tb <= '0';
				R6_drive_tb <= '0';
				R7_drive_tb <= '0';
				R8_drive_tb <= '0';
				R9_drive_tb <= '0';
				R10_drive_tb <= '0';
				R11_drive_tb <= '0';
				R12_drive_tb <= '0';
				R13_drive_tb <= '0';
				R14_drive_tb <= '0';
				R15_drive_tb <= '0';
				HI_drive_tb <= '0';
				LO_drive_tb <= '0';
				Zhigh_drive_tb <= '0';
				Zlow_drive_tb <= '0';
				PC_drive_tb <= '0';
				MDR_drive_tb <= '0';
				R0_en_inpin_tb <= '0';
				R1_en_inpin_tb <= '0';
				R2_en_inpin_tb <= '0';
				R3_en_inpin_tb <= '0';
				R4_en_inpin_tb <= '0';
				R5_en_inpin_tb <= '0';
				R6_en_inpin_tb <= '0';
				R7_en_inpin_tb <= '0';
				R8_en_inpin_tb <= '0';
				R9_en_inpin_tb <= '0';
				R10_en_inpin_tb <= '0';
				R11_en_inpin_tb <= '0';
				R12_en_inpin_tb <= '0';
				R13_en_inpin_tb <= '0';
				R14_en_inpin_tb <= '0';
				R15_en_inpin_tb <= '0';
				HI_en_inpin_tb <= '0';
				LO_en_inpin_tb <= '0';
				PC_en_inpin_tb <= '0';
				IR_en_inpin_tb <= '0';
				Zhigh_en_inpin_tb <= '0';
				Zlow_en_inpin_tb <= '0';
				Y_en_inpin_tb <= '0';
				MDR_en_inpin_tb <= '0';
				read_mem_inpin_tb <= '0';
				MAR_en_tb <= '0';
				ADD_OP_tb <= '0';
				SUB_OP_tb <= '0';
				MUL_OP_tb <= '0';
				DIV_OP_tb <= '0';
				AND_OP_tb <= '0';
				OR_OP_tb <= '0';
				SHR_OP_tb <= '0';
				SHL_OP_tb <= '0';
				ROR_OP_tb <= '0';
				ROL_OP_tb <= '0';
				NEG_OP_tb <= '0';
				NOT_OP_tb <= '0';
				
				-- Clear the system for half cycle before starting.
				Clear_tb <= '1', '0' after 20 ns;

			WHEN Reg_load1 =>
				-- Put value for R1 into MDR
				Mdatain_tb <= x"0000FFFF";
				read_mem_inpin_tb <= '1';
				MDR_en_inpin_tb <= '1';
			WHEN Reg_load2 =>
				-- Put MDR into R1
				R1_en_inpin_tb <= '1';
				MDR_drive_tb <= '1';
				-- Load value for R2 into MDR
				Mdatain_tb <= x"00000001";
				read_mem_inpin_tb <= '1';
				MDR_en_inpin_tb <= '1';
			WHEN Reg_load3 =>
				-- Reset previous state signals
				R1_en_inpin_tb <= '0';
				-- Put MDR into R2
				R2_en_inpin_tb <= '1';
				MDR_drive_tb <= '1';
				-- Load value for R3 into MDR
				Mdatain_tb <= x"00000001";
				read_mem_inpin_tb <= '1';
				MDR_en_inpin_tb <= '1';
			WHEN Reg_load4 =>
				-- Reset previous state signals
				R2_en_inpin_tb <= '0';
				-- Put MDR into R3
				R3_en_inpin_tb <= '1';
				MDR_drive_tb <= '1';
			WHEN T0 =>
				-- Reset previous state signals
				R3_en_inpin_tb <= '0';
				MDR_drive_tb <= '0';
				-- Zlow <-- PC + 4
				PC_drive_tb <= '1';
				INC_PC_tb <= '1';
				ADD_OP_tb <= '1';
				Zlow_en_inpin_tb <= '1';
			WHEN T1 =>
				-- Reset previous state signals
				PC_drive_tb <= '0';
				INC_PC_tb <= '0';
				ADD_OP_tb <= '0';
				Zlow_en_inpin_tb <= '0';
				-- PC <-- Zlow; MAR <-- Zlow
				Zlow_drive_tb <= '1';
				PC_en_inpin_tb <= '1';
				MAR_en_tb <= '1';
				-- MDR <-- Mdatain (instruction from mem)
				read_mem_inpin_tb <= '1';
				MDR_en_inpin_tb <= '1';
				Mdatain_tb <= x"28918000"; -- opcode
			WHEN T2 =>
				-- Reset previous signals
				Zlow_drive_tb <= '0';
				PC_en_inpin_tb <= '0';
				MAR_en_tb <= '0';
				read_mem_inpin_tb <= '0';
				MDR_en_inpin_tb <= '0';
				-- IR <-- MDR
				MDR_drive_tb <= '1';
				IR_en_inpin_tb <= '1';
			WHEN T3 =>
				-- Reset previous signals
				MDR_drive_tb <= '0';
				IR_en_inpin_tb <= '0';
				-- Y <-- R2
				R2_drive_tb <= '1';
				Y_en_inpin_tb <= '1';
			WHEN T4 =>
				-- Reset previous signals
				R2_drive_tb <= '0';
				Y_en_inpin_tb <= '0';
				-- Zlow <-- R2 AND R3
				R3_drive_tb <= '1';
				ROR_OP_tb <= '1';
				Zlow_en_inpin_tb <= '1';
			WHEN T5 =>
				-- Reset previous signals
				R3_drive_tb <= '0';
				ROR_OP_tb <= '0';
				Zlow_en_inpin_tb <= '0';
				-- R1 <-- Zlow
				Zlow_drive_tb <= '1';
				R1_en_inpin_tb <= '1';
			WHEN OTHERS =>
		END CASE;
	END PROCESS;
END ARCHITECTURE ror_datapath_tb_arch;