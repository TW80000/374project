library ieee;
use ieee.std_logic_1164.all;

entity ldr_tb is
end entity;

architecture arch of ldr_tb is
	component datapath is
	port
	(
		INC_PC :  IN  STD_LOGIC;
		Gra :  IN  STD_LOGIC;
		Grb :  IN  STD_LOGIC;
		Grc :  IN  STD_LOGIC;
		Rin :  IN  STD_LOGIC;
		Rout :  IN  STD_LOGIC;
		Cout : IN STD_LOGIC;
		BAout :  IN  STD_LOGIC;
		HI_drive :  IN  STD_LOGIC;
		LO_drive :  IN  STD_LOGIC;
		Zhigh_drive :  IN  STD_LOGIC;
		Zlow_drive :  IN  STD_LOGIC;
		PC_drive :  IN  STD_LOGIC;
		MDR_drive :  IN  STD_LOGIC;
		HI_en_inpin :  IN  STD_LOGIC;
		LO_en_inpin :  IN  STD_LOGIC;
		PC_en_inpin :  IN  STD_LOGIC;
		IR_en_inpin :  IN  STD_LOGIC;
		Zhigh_en_inpin :  IN  STD_LOGIC;
		Zlow_en_inpin :  IN  STD_LOGIC;
		Y_en_inpin :  IN  STD_LOGIC;
		MDR_en_inpin :  IN  STD_LOGIC;
		OutPort_en : IN STD_LOGIC;
		InPort_drive : IN STD_LOGIC;
		read_mem :  IN  STD_LOGIC;
		write_mem : IN  STD_LOGIC;
		MAR_en :  IN  STD_LOGIC;
		CON_en :  IN  STD_LOGIC;
		ADD_OP :  IN  STD_LOGIC;
		SUB_OP :  IN  STD_LOGIC;
		MUL_OP :  IN  STD_LOGIC;
		DIV_OP :  IN  STD_LOGIC;
		AND_OP :  IN  STD_LOGIC;
		OR_OP  :  IN  STD_LOGIC;
		SHR_OP :  IN  STD_LOGIC;
		SHL_OP :  IN  STD_LOGIC;
		ROR_OP :  IN  STD_LOGIC;
		ROL_OP :  IN  STD_LOGIC;
		NEG_OP :  IN  STD_LOGIC;
		NOT_OP :  IN  STD_LOGIC;
		Clear  :  IN  STD_LOGIC;
		Clock  :  IN  STD_LOGIC;
		IO_input : IN STD_LOGIC_VECTOR(31 downto 0);
		IO_output : OUT STD_LOGIC_VECTOR(31 downto 0);
		CON_FF : OUT STD_LOGIC
	);
	end component;
	
	signal Inc_PC_tb : STD_LOGIC;
	signal Gra_tb : STD_LOGIC;
	signal Grb_tb : STD_LOGIC;
	signal Grc_tb : STD_LOGIC;
	signal Rin_tb : STD_LOGIC;
	signal Rout_tb : STD_LOGIC;
	signal Cout_tb : STd_LOGIC;
	signal BAout_tb : STD_LOGIC;
	signal HI_drive_tb : std_logic;
	signal LO_drive_tb : std_logic;
	signal Zhigh_drive_tb : std_logic;
	signal Zlow_drive_tb : std_logic;
	signal PC_drive_tb : std_logic;
	signal MDR_drive_tb : std_logic;
	signal HI_en_inpin_tb : std_logic;
	signal LO_en_inpin_tb : std_logic;
	signal PC_en_inpin_tb : std_logic;
	signal IR_en_inpin_tb : std_logic;
	signal Zhigh_en_inpin_tb : std_logic;
	signal Zlow_en_inpin_tb : std_logic;
	signal Y_en_inpin_tb : std_logic;
	signal MDR_en_inpin_tb : std_logic;
	signal OutPort_en_tb : std_logic;
	signal InPort_drive_tb : std_logic;
	signal read_mem_tb : std_logic;
	signal write_mem_tb : std_logic;
	signal MAR_en_tb : std_logic;
	signal CON_en_tb : std_logic;
	signal ADD_OP_tb : STD_LOGIC;
	signal SUB_OP_tb : STD_LOGIC;
	signal MUL_OP_tb : STD_LOGIC;
	signal DIV_OP_tb : STD_LOGIC;
	signal AND_OP_tb : STD_LOGIC;
	signal OR_OP_tb  : STD_LOGIC;
	signal SHR_OP_tb : STD_LOGIC;
	signal SHL_OP_tb : STD_LOGIC;
	signal ROR_OP_tb : STD_LOGIC;
	signal ROL_OP_tb : STD_LOGIC;
	signal NEG_OP_tb : STD_LOGIC;
	signal NOT_OP_tb : STD_LOGIC;
	signal Clear_tb : std_logic;
	signal Clock_tb : std_logic;
	signal IO_input_tb : std_logic_vector(31 downto 0);
	signal IO_output_tb : std_logic_vector(31 downto 0);
	signal CON_FF_tb : std_logic;
	TYPE State IS (default, IO_LOAD, PC_LOAD, IO_LOAD_2, IR_LOAD, RB_LOAD, T0, T1, T2, T3, T4, T5, T6, T7, T8);
	SIGNAL Present_state: State := default;
begin

DUT : datapath
port map
(
	INC_PC => INC_PC_tb,
	Gra => Gra_tb,
	Grb => Grb_tb,
	Grc => Grc_tb,
	Rin => Rin_tb,
	Rout => Rout_tb,
	Cout => Cout_tb,
	BAout => BAout_tb,
   HI_drive => HI_drive_tb,
   LO_drive => LO_drive_tb,
   Zhigh_drive => Zhigh_drive_tb,
   Zlow_drive => Zlow_drive_tb,
   PC_drive => PC_drive_tb,
   MDR_drive => MDR_drive_tb,
   HI_en_inpin => HI_en_inpin_tb,
   LO_en_inpin => LO_en_inpin_tb,
   PC_en_inpin => PC_en_inpin_tb,
   IR_en_inpin => IR_en_inpin_tb,
   Zhigh_en_inpin => Zhigh_en_inpin_tb,
   Zlow_en_inpin => Zlow_en_inpin_tb,
   Y_en_inpin => Y_en_inpin_tb,
   MDR_en_inpin => MDR_en_inpin_tb,
   OutPort_en => OutPort_en_tb,
   InPort_drive => InPort_drive_tb,
   read_mem => read_mem_tb,
   write_mem => write_mem_tb,
   MAR_en => MAR_en_tb,
   CON_en => CON_en_tb,
	ADD_OP => ADD_OP_tb,
	SUB_OP => SUB_OP_tb,
	MUL_OP => MUL_OP_tb,
	DIV_OP => DIV_OP_tb,
	AND_OP => AND_OP_tb,
	OR_OP  => OR_OP_tb,
	SHR_OP => SHR_OP_tb,
	SHL_OP => SHL_OP_tb,
	ROR_OP => ROR_OP_tb,
	ROL_OP => ROL_OP_tb,
	NEG_OP => NEG_OP_tb,
	NOT_OP => NOT_OP_tb,
   Clear  => Clear_tb,
   Clock  => Clock_tb,
   IO_input => IO_input_tb,
   IO_output => IO_output_tb,
   CON_FF => CON_FF_tb
);

Clock_process: PROCESS
BEGIN
	Clock_tb <= '1', '0' after 20 ns;
	Wait for 40 ns;
END PROCESS Clock_process;

PROCESS(Clock_tb) -- finite state machine
BEGIN
	IF (Clock_tb'EVENT AND Clock_tb = '1') THEN -- if clock rising-edge
		CASE Present_state IS
			WHEN Default =>
				Present_state <= IO_LOAD;
			WHEN IO_LOAD =>
				Present_state <= PC_LOAD;
			WHEN PC_LOAD =>
				Present_state <= IO_LOAD_2;
			WHEN IO_LOAD_2 =>
				Present_state <= IR_LOAD;
			WHEN IR_LOAD =>
				Present_state <= RB_LOAD;
			WHEN RB_LOAD =>
				Present_state <= T0;
			WHEN T0 =>
				Present_state <= T1;
			WHEN T1 =>
				Present_state <= T2;
			WHEN T2 =>
				Present_state <= T3;
			WHEN T3 =>
				Present_state <= T4;
			WHEN T4 =>
				Present_state <= T5;
			WHEN T5 =>
				Present_state <= T6;
			WHEN T6 =>
				Present_state <= T7;
			WHEN T7 =>
				Present_state <= T8;
			WHEN OTHERS =>
		END CASE;
	END IF;
END PROCESS;
	
PROCESS(Present_state) -- do the required job in each state
BEGIN
	CASE Present_state IS -- assert the required signals in each clock cycle
		WHEN Default =>
			INC_PC_tb <= '0';
			Gra_tb <= '0';
			Grb_tb <= '0';
			Grc_tb <= '0';
			Rin_tb <= '0';
			Rout_tb <= '0';
			Cout_tb <= '0';
			BAout_tb <= '0';
			HI_drive_tb <= '0';
			LO_drive_tb <= '0';
			Zhigh_drive_tb <= '0';
			Zlow_drive_tb <= '0';
			PC_drive_tb <= '0';
			MDR_drive_tb <= '0';
			HI_en_inpin_tb <= '0';
			LO_en_inpin_tb <= '0';
			PC_en_inpin_tb <= '0';
			IR_en_inpin_tb <= '0';
			Zhigh_en_inpin_tb <= '0';
			Zlow_en_inpin_tb <= '0';
			Y_en_inpin_tb <= '0';
			MDR_en_inpin_tb <= '0';
			OutPort_en_tb <= '0';
			InPort_drive_tb <= '0';
			read_mem_tb <= '0';
			write_mem_tb <= '0';
			MAR_en_tb <= '0';
			CON_en_tb <= '0';
			ADD_OP_tb <= '0';
			SUB_OP_tb <= '0';
			MUL_OP_tb <= '0';
			DIV_OP_tb <= '0';
			AND_OP_tb <= '0';
			OR_OP_tb  <= '0';
			SHR_OP_tb <= '0';
			SHL_OP_tb <= '0';
			ROR_OP_tb <= '0';
			ROL_OP_tb <= '0';
			NEG_OP_tb <= '0';
			NOT_OP_tb <= '0';
			IO_output_tb <= x"00000000";
			CON_FF_tb <= '0';
			
			-- Clear the system for half cycle before starting.
			Clear_tb <= '1', '0' after 20 ns;

		WHEN IO_LOAD =>
			-- ldr   r0/ra     65
			-- 00011 0000 0000 0000000000000110111
			-- 0001 1000 0000 0000 0000 0000 0100 0001
			-- 1    8    0    0    0    0    4    1
			-- 18000037
			-- InPort <-- The outside world
			IO_input_tb <= x"00000004";  -- {{ ldr R0, $65 }} is at this address in meminit.mif
		WHEN PC_LOAD =>
			-- Load the address of the instruction into the PC via the input port
			-- PC <-- Input IO Port
			InPort_drive_tb <= '1';
			PC_en_inpin_tb <= '1';
		WHEN T0 =>
			-- Reset previous state signals
			PC_drive_tb <= '0';
			Rin_tb <= '0';
			Grb_tb <= '0';
			InPort_drive_tb <= '0';
			-- Zlow <-- PC + 4
			-- MAR <-- PC
			PC_drive_tb <= '1';
			MAR_en_tb <= '1';
			INC_PC_tb <= '1';
			ADD_OP_tb <= '1';
			Zlow_en_inpin_tb <= '1';
			-- Assert read_mem now so ready to read next cycle
			read_mem_tb <= '1';
		WHEN T1 =>
			-- Reset previous state signals
			PC_drive_tb <= '0';
			INC_PC_tb <= '0';
			ADD_OP_tb <= '0';
			Zlow_en_inpin_tb <= '0';
			-- PC <-- Zlow
			-- MAR <-- Zlow
			-- Y <-- Zlow
			Zlow_drive_tb <= '1';
			PC_en_inpin_tb <= '1';
			MAR_en_tb <= '1';
			Y_en_inpin_tb <= '1';
		WHEN T2 =>
			-- Reset previous signals
			Zlow_drive_tb <= '0';
			PC_en_inpin_tb <= '0';
			MAR_en_tb <= '0';
			MDR_en_inpin_tb <= '0';
			Y_en_inpin_tb <= '0';
			-- MDR <-- (instruction from mem)
			MDR_en_inpin_tb <= '1';
		WHEN T3 =>
			-- Reset previous signals
			read_mem_tb <= '0';
			MDR_en_inpin_tb <= '0';
			-- IR <-- MDR
			MDR_drive_tb <= '1';
			IR_en_inpin_tb <= '1';
		WHEN T4 =>
			-- Reset previous signals
			MDR_drive_tb <= '0';
			IR_en_inpin_tb <= '0';
			Grb_tb <= '0';
			BAout_tb <= '0';
			Y_en_inpin_tb <= '0';
			-- Zlow <-- Y + Cout
			Cout_tb <= '1';
			ADD_OP_tb <= '1';
			Zlow_en_inpin_tb <= '1';
		WHEN T5 =>
			-- Reset previous signals
			Cout_tb <= '0';
			AND_OP_tb <= '0';
			Zlow_en_inpin_tb <= '0';
			-- MAR <-- Zlow
			Zlow_drive_tb <= '1';
			MAR_en_tb <= '1';
		WHEN T6 =>
			-- Reset previous signals
			Zlow_drive_tb <= '0';
			MAR_en_tb <= '0';
			-- Assert read_mem now to read next cycle (because read enable input of RAM is registered)
			-- Can't do it in the previous cycle because we need to wait for MAR's value to be updated
			read_mem_tb <= '1';
		WHEN T7 =>
			-- MDR <-- (Data from memory)
			MDR_en_inpin_tb <= '1';
		WHEN T8 =>
			-- Reset previous signals
			read_mem_tb <= '0';
			MDR_en_inpin_tb <= '0';
			-- Ra <-- MDR
			MDR_drive_tb <= '1';
			Gra_tb <= '1';
			Rin_tb <= '1';
		WHEN OTHERS =>
	END CASE;
END PROCESS;

end architecture arch;