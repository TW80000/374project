LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY carry_lookahead_adder_tb IS
END;

ARCHITECTURE carry_lookahead_adder_tb_arch OF carry_lookahead_adder_tb IS
	SIGNAL X_tb 			: std_logic_vector(15 DOWNTO 0);
	SIGNAL Y_tb 			: std_logic_vector(15 DOWNTO 0);
	SIGNAL CarryIn_tb 	: std_logic;
	SIGNAL CarryOut_tb 	: std_logic;
	SIGNAL Sum_tb 			: std_logic_vector(15 DOWNTO 0);

	COMPONENT carry_lookahead_adder
		PORT 
		(
			X			: IN std_logic_vector(15 DOWNTO 0);
			Y			: IN std_logic_vector(15 DOWNTO 0);
			CarryIn	: IN std_logic;
			CarryOut	: OUT std_logic;
			Sum		: OUT std_logic_vector(15 DOWNTO 0)		
		);
	END COMPONENT carry_lookahead_adder;
 
BEGIN
	DUT : carry_lookahead_adder
	PORT MAP
	(
		X 			=> X_tb, 
		Y			=> Y_tb, 
		CarryIn 	=> CarryIn_tb,
		CarryOut => CarryOut_tb,
		Sum		=> Sum_tb
	);
 
	PROCESS
	BEGIN
		X_tb 				<= X"0004";
		Y_tb 				<= X"0020";
		CarryIn_tb 		<= '0';
		WAIT;
	END PROCESS;
END ARCHITECTURE carry_lookahead_adder_tb_arch;