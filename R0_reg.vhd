LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE STD.textio.ALL;
USE IEEE.std_logic_textio.ALL;

ENTITY R0_reg IS
	PORT
	(
		BAout        : IN std_logic;
		reg_in       : IN std_logic_vector(31 DOWNTO 0);
		clr, le, clk : IN std_logic;
		reg_out      : OUT std_logic_vector(31 DOWNTO 0)
	);
END R0_reg;

ARCHITECTURE R0_reg_arch OF R0_reg IS
BEGIN
	reg: PROCESS(clk, clr)
	BEGIN
		IF (clr = '1') THEN
			reg_out <= (OTHERS => '0');
		ELSIF (clk'EVENT and clk = '1') THEN
			IF (le = '1') THEN
				reg_out(31) <= reg_in(31) and not BAout;
				reg_out(30) <= reg_in(30) and not BAout;
				reg_out(29) <= reg_in(29) and not BAout;
				reg_out(28) <= reg_in(28) and not BAout;
				reg_out(27) <= reg_in(27) and not BAout;
				reg_out(26) <= reg_in(26) and not BAout;
				reg_out(25) <= reg_in(25) and not BAout;
				reg_out(24) <= reg_in(24) and not BAout;
				reg_out(23) <= reg_in(23) and not BAout;
				reg_out(22) <= reg_in(22) and not BAout;
				reg_out(21) <= reg_in(21) and not BAout;
				reg_out(20) <= reg_in(20) and not BAout;
				reg_out(19) <= reg_in(19) and not BAout;
				reg_out(18) <= reg_in(18) and not BAout;
				reg_out(17) <= reg_in(17) and not BAout;
				reg_out(16) <= reg_in(16) and not BAout;
				reg_out(15) <= reg_in(15) and not BAout;
				reg_out(14) <= reg_in(14) and not BAout;
				reg_out(13) <= reg_in(13) and not BAout;
				reg_out(12) <= reg_in(12) and not BAout;
				reg_out(11) <= reg_in(11) and not BAout;
				reg_out(10) <= reg_in(10) and not BAout;
				reg_out(9) <= reg_in(9) and not BAout;
				reg_out(8) <= reg_in(8) and not BAout;
				reg_out(7) <= reg_in(7) and not BAout;
				reg_out(6) <= reg_in(6) and not BAout;
				reg_out(5) <= reg_in(5) and not BAout;
				reg_out(4) <= reg_in(4) and not BAout;
				reg_out(3) <= reg_in(3) and not BAout;
				reg_out(2) <= reg_in(2) and not BAout;
				reg_out(1) <= reg_in(1) and not BAout;
				reg_out(0) <= reg_in(0) and not BAout;
			END IF;
		END IF;
	END PROCESS;
END R0_reg_arch;