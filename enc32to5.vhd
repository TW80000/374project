LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY enc32to5 IS
    PORT 
    (
        IN0, IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12,
		  IN13, IN14, IN15, IN16, IN17, IN18, IN19, IN20, IN21, IN22, IN23, IN24,
		  IN25, IN26, IN27, IN28, IN29, IN30, IN31
						: IN std_logic;
        ENC_OUT   : OUT std_logic_vector(4 DOWNTO 0)
    );
END enc32to5;

ARCHITECTURE enc32to5_arch OF enc32to5 IS
BEGIN
    ENC_OUT <= "00000" WHEN IN0 = '1' ELSE
               "00001" WHEN IN1 = '1' ELSE
               "00010" WHEN IN2 = '1' ELSE
               "00011" WHEN IN3 = '1' ELSE
               "00100" WHEN IN4 = '1' ELSE
               "00101" WHEN IN5 = '1' ELSE
               "00110" WHEN IN6 = '1' ELSE
               "00111" WHEN IN7 = '1' ELSE
               "01000" WHEN IN8 = '1' ELSE
               "01001" WHEN IN9 = '1' ELSE
               "01010" WHEN IN10 = '1' ELSE
               "01011" WHEN IN11 = '1' ELSE
               "01100" WHEN IN12 = '1' ELSE
               "01101" WHEN IN13 = '1' ELSE
               "01110" WHEN IN14 = '1' ELSE
               "01111" WHEN IN15 = '1' ELSE
               "10000" WHEN IN16 = '1' ELSE
               "10001" WHEN IN17 = '1' ELSE
               "10010" WHEN IN18 = '1' ELSE
               "10011" WHEN IN19 = '1' ELSE
               "10100" WHEN IN20 = '1' ELSE
               "10101" WHEN IN21 = '1' ELSE
               "10110" WHEN IN22 = '1' ELSE
               "10111" WHEN IN23 = '1' ELSE
               "11000" WHEN IN24 = '1' ELSE
               "11001" WHEN IN25 = '1' ELSE
               "11010" WHEN IN26 = '1' ELSE
               "11011" WHEN IN27 = '1' ELSE
               "11100" WHEN IN28 = '1' ELSE
               "11101" WHEN IN29 = '1' ELSE
               "11110" WHEN IN30 = '1' ELSE
               "11111" WHEN IN31 = '1' ELSE
					"XXXXX";

END enc32to5_arch;