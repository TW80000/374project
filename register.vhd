LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE STD.textio.ALL;
USE IEEE.std_logic_textio.ALL;

ENTITY reg32 IS
	PORT
	(
		reg_in       : IN std_logic_vector(31 DOWNTO 0);
		clr, le, clk : IN std_logic;
		reg_out      : OUT std_logic_vector(31 DOWNTO 0)
	);
END reg32;

ARCHITECTURE reg32_arch OF reg32 IS
BEGIN
	reg: PROCESS(clk, clr)
	BEGIN
		IF (clr = '1') THEN
			reg_out <= (OTHERS => '0');
		ELSIF (clk'EVENT AND clk = '1') THEN
			IF (le = '1') THEN
				reg_out <= reg_in;
			END IF;
		END IF;
	END PROCESS;
END reg32_arch;