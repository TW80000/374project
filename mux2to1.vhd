LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE STD.textio.ALL;
USE IEEE.std_logic_textio.ALL;

ENTITY mux2to1 IS
    PORT 
    (
        IN0, IN1  : IN std_logic_vector(31 DOWNTO 0);
        SEL       : IN std_logic;
        MUX_OUT   : OUT std_logic_vector (31 DOWNTO 0)
    );
END mux2to1;

ARCHITECTURE Behavioural OF mux2to1 IS
BEGIN
	MUX_OUT <= IN0 WHEN (SEL = '0') ELSE IN1;
END Behavioural;