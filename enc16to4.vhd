LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY enc16to4 IS
    PORT 
    (
        IN0, IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12,
		  IN13, IN14, IN15	: IN std_logic;
        ENC_OUT				: OUT std_logic_vector(3 DOWNTO 0)
    );
END enc16to4;

ARCHITECTURE enc16to4_arch OF enc16to4 IS
BEGIN
    ENC_OUT <= "0000" WHEN IN0 = '1' ELSE
               "0001" WHEN IN1 = '1' ELSE
               "0010" WHEN IN2 = '1' ELSE
               "0011" WHEN IN3 = '1' ELSE
               "0100" WHEN IN4 = '1' ELSE
               "0101" WHEN IN5 = '1' ELSE
               "0110" WHEN IN6 = '1' ELSE
               "0111" WHEN IN7 = '1' ELSE
               "1000" WHEN IN8 = '1' ELSE
               "1001" WHEN IN9 = '1' ELSE
               "1010" WHEN IN10 = '1' ELSE
               "1011" WHEN IN11 = '1' ELSE
               "1100" WHEN IN12 = '1' ELSE
               "1101" WHEN IN13 = '1' ELSE
               "1110" WHEN IN14 = '1' ELSE
               "1111" WHEN IN15 = '1' ELSE
					"XXXX";

END enc16to4_arch;