LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE STD.textio.ALL;
USE IEEE.std_logic_textio.ALL;

ENTITY reg1 IS
	PORT
	(
		reg_in       : IN std_logic;
		clr, le, clk : IN std_logic;
		reg_out      : OUT std_logic
	);
END reg1;

ARCHITECTURE reg1_arch OF reg1 IS
BEGIN
	reg: PROCESS(clk, clr)
	BEGIN
		IF (clr = '1') THEN
			reg_out <= '0';
		ELSIF (clk'EVENT AND clk = '1') THEN
			IF (le = '1') THEN
				reg_out <= reg_in;
			END IF;
		END IF;
	END PROCESS;
END reg1_arch;