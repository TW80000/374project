library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity d_flipflop is
   port
   (
      clk : in std_logic;
      ce  : in std_logic;
      d : in std_logic;
      q : out std_logic
   );
end entity d_flipflop;
 
architecture Behavioral of d_flipflop is
begin
   process (clk) is
   begin
      if rising_edge(clk) then  
         if (ce='1') then
            q <= d;
			else
				q <= '0';
         end if;
      end if;
   end process;
end architecture Behavioral;