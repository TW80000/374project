LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY four_bit_adder IS
	PORT 
	(
		xin			: IN std_logic_vector(3 DOWNTO 0);
		yin			: IN std_logic_vector(3 DOWNTO 0);
		Cin       	: IN std_logic;
		Cout      	: OUT std_logic;
		Sout      	: OUT std_logic_vector(3 DOWNTO 0)
	);
END four_bit_adder;

ARCHITECTURE four_bit_adder_arch OF four_bit_adder IS
	SIGNAL Gout, Pout : std_logic_vector(3 DOWNTO 0);
	SIGNAL Ctemp : std_logic_vector(4 DOWNTO 0);
BEGIN
	Ctemp(0) <= Cin;
	mapValues : FOR i IN 0 TO 3 GENERATE
		Pout(i) <= xin(i) XOR yin(i);
		Gout(i) <= xin(i) AND yin(i);
		Sout(i) <= Ctemp(i) XOR Pout(i);
		Ctemp(i + 1) <= Gout(i) OR (Pout(i) AND Ctemp(i));
	END GENERATE mapValues;
 --only carry that we care about, rest are internal carrys that feed into the next
	Cout <= Gout(3) OR (Pout(3) AND Ctemp(3));

END four_bit_adder_arch;