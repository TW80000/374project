LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY select_encode IS
	PORT 
	(
		IR												: IN std_logic_vector(31 DOWNTO 0);
		Gra, Grb, Grc, Rin, Rout, BAout		: IN std_logic;
		C_Sign_Ext									: OUT std_logic_vector(31 DOWNTO 0);
		R15_in, R14_in, R13_in, R12_in		: OUT std_logic;
		R11_in, R10_in, R9_in, R8_in			: OUT std_logic;
		R7_in, R6_in, R5_in, R4_in				: OUT std_logic;
		R3_in, R2_in, R1_in, R0_in				: OUT std_logic;
		R15_out, R14_out, R13_out, R12_out	: OUT std_logic;
		R11_out, R10_out, R9_out, R8_out		: OUT std_logic;
		R7_out, R6_out, R5_out, R4_out		: OUT std_logic;
		R3_out, R2_out, R1_out, R0_out		: OUT std_logic
	);
END select_encode;

ARCHITECTURE select_encode_arch OF select_encode IS 

SIGNAL or_gate										: std_logic;
SIGNAL decoder_input								: std_logic_vector(3 DOWNTO 0);
SIGNAL decoder_output							: std_logic_vector(15 DOWNTO 0);

COMPONENT dec_4_16 is 
	PORT
	(
		a  : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		b  : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;
	
BEGIN
	
	decoder:dec_4_16 
	PORT MAP
	(
		decoder_input,
		decoder_output
	);
	
	decoder_input	<= (IR(26 DOWNTO 23) AND (Gra & Gra & Gra & Gra)) OR 
							(IR(22 DOWNTO 19) AND (Grb & Grb & Grb & Grb)) OR 
							(IR(18 DOWNTO 15) AND (Grc & Grc & Grc & Grc));
	or_gate			<=	Rout OR BAout;
	C_Sign_Ext		<= (IR(18) & IR(18) & IR(18) & IR(18) & IR(18) & 
							 IR(18) & IR(18) & IR(18) & IR(18) & IR(18) & 
							 IR(18) & IR(18) & IR(18) & IR(18 DOWNTO 0));
	R15_in			<= decoder_output(15) AND Rin;
	R14_in			<= decoder_output(14) AND Rin;
	R13_in			<= decoder_output(13) AND Rin;
	R12_in			<= decoder_output(12) AND Rin;
	R11_in			<= decoder_output(11) AND Rin;
	R10_in			<= decoder_output(10) AND Rin;
	R9_in				<= decoder_output(9) AND Rin;
	R8_in				<= decoder_output(8) AND Rin;
	R7_in				<= decoder_output(7) AND Rin;
	R6_in				<= decoder_output(6) AND Rin;
	R5_in				<= decoder_output(5) AND Rin;
	R4_in				<= decoder_output(4) AND Rin;
	R3_in				<= decoder_output(3) AND Rin;
	R2_in				<= decoder_output(2) AND Rin;
	R1_in				<= decoder_output(1) AND Rin;
	R0_in				<= decoder_output(0) AND Rin;
	R15_out			<= decoder_output(15) AND or_gate;
	R14_out			<= decoder_output(14) AND or_gate;
	R13_out        <= decoder_output(13) AND or_gate;
	R12_out        <= decoder_output(12) AND or_gate;
	R11_out        <= decoder_output(11) AND or_gate;
	R10_out        <= decoder_output(10) AND or_gate;
	R9_out         <= decoder_output(9) AND or_gate;
	R8_out         <= decoder_output(8) AND or_gate;
	R7_out         <= decoder_output(7) AND or_gate;
	R6_out         <= decoder_output(6) AND or_gate;
	R5_out         <= decoder_output(5) AND or_gate;
	R4_out         <= decoder_output(4) AND or_gate;
	R3_out         <= decoder_output(3) AND or_gate;
	R2_out         <= decoder_output(2) AND or_gate;
	R1_out         <= decoder_output(1) AND or_gate;
	R0_out         <= decoder_output(0) AND or_gate;
	
END select_encode_arch;