LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY booth_multiplier IS
	PORT 
	(
		multiplier, multiplicand  : IN std_logic_vector(31 DOWNTO 0);
		product     : OUT std_logic_vector(63 DOWNTO 0)
	);
END booth_multiplier;
 
ARCHITECTURE booth_multiplier_arch OF booth_multiplier IS 
BEGIN
	PROCESS (multiplier, multiplicand)
	VARIABLE var_product : std_logic_vector(64 DOWNTO 0);
	VARIABLE var_multiplier, var_multiplicand : std_logic_vector(31 DOWNTO 0);
	VARIABLE i : INTEGER;
	BEGIN
		var_product := "00000000000000000000000000000000000000000000000000000000000000000";
		var_multiplier := multiplicand;
		var_product(32 DOWNTO 1) := multiplier;
 
		FOR i IN 0 TO 31 LOOP
			IF (var_product(1) = '1' AND var_product(0) = '0') THEN
				var_multiplicand := (var_product(64 DOWNTO 33));
				var_product(64 DOWNTO 33) := (var_multiplicand - var_multiplier);
 
			ELSIF (var_product(1) = '0' AND var_product(0) = '1') THEN
				var_multiplicand := (var_product(64 DOWNTO 33));
				var_product(64 DOWNTO 33) := (var_multiplicand + var_multiplier);
			
			END IF;
 
			var_product(63 DOWNTO 0) := var_product(64 DOWNTO 1);
 
		END LOOP;
 
		product(63 DOWNTO 0) <= var_product(64 DOWNTO 1);
 
	END PROCESS;
 
END booth_multiplier_arch;