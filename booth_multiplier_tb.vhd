LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
 
ENTITY booth_multiplier_tb IS
END;

ARCHITECTURE booth_multiplier_tb_arch OF booth_multiplier_tb IS
	SIGNAL multiplier_tb : std_logic_vector(31 DOWNTO 0);
	SIGNAL multiplicand_tb : std_logic_vector(31 DOWNTO 0);
	SIGNAL product_tb : std_logic_vector(63 DOWNTO 0);

	COMPONENT booth_multiplier
		PORT 
		(
			multiplier, multiplicand  : IN std_logic_vector(31 DOWNTO 0);
			product     : OUT std_logic_vector(63 DOWNTO 0)
		);
	END COMPONENT booth_multiplier;
 
BEGIN
	DUT : booth_multiplier
	PORT MAP
	(
		multiplier    => multiplier_tb, 
		multiplicand  => multiplicand_tb, 
		product       => product_tb
	);
 
	PROCESS
	BEGIN
		multiplier_tb <= X"00000011";
		multiplicand_tb <= X"00000011";
		WAIT;
	END PROCESS;

END ARCHITECTURE booth_multiplier_tb_arch;