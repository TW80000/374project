LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY carry_lookahead_adder IS
	PORT 
	(
		X				: IN std_logic_vector(15 DOWNTO 0);
		Y				: IN std_logic_vector(15 DOWNTO 0);
		CarryIn   	: IN std_logic;
		CarryOut  	: OUT std_logic;
		Sum       	: OUT std_logic_vector(15 DOWNTO 0)

	);
END carry_lookahead_adder;

ARCHITECTURE carry_lookahead_adder_arch OF carry_lookahead_adder IS

	COMPONENT four_bit_adder IS
		PORT 
		(
			xin			: IN std_logic_vector(3 DOWNTO 0);
			yin			: IN std_logic_vector(3 DOWNTO 0);
			Cin       	: IN std_logic;
			Cout      	: OUT std_logic;
			Sout      	: OUT std_logic_vector(3 DOWNTO 0)
		);
	END COMPONENT;

	SIGNAL CarryOut_CarryLookAhead_1, CarryOut_CarryLookAhead_2, CarryOut_CarryLookAhead_3 : std_logic;
	SIGNAL Sum_CarryLookAhead_1, Sum_CarryLookAhead_2, Sum_CarryLookAhead_3, Sum_CarryLookAhead_4 : std_logic_vector(3 DOWNTO 0);

BEGIN
	Carry_LookAhead_1 : four_bit_adder
	PORT MAP
	(
		X(3 DOWNTO 0), 
		Y(3 DOWNTO 0), 
		CarryIn, 
		CarryOut_CarryLookAhead_1, 
		Sum_CarryLookAhead_1
	);

	Carry_LookAhead_2  : four_bit_adder
	PORT MAP
	( 
		X(7 DOWNTO 4), 
		Y(7 DOWNTO 4), 
		CarryOut_CarryLookAhead_1,
		CarryOut_CarryLookAhead_2, 
		Sum_CarryLookAhead_2
	);

	Carry_LookAhead_3  : four_bit_adder
	PORT MAP
	(
		X(11 DOWNTO 8), 
		Y(11 DOWNTO 8), 
		CarryOut_CarryLookAhead_2, 
		CarryOut_CarryLookAhead_3, 
		Sum_CarryLookAhead_3
	);

	Carry_LookAhead_4  : four_bit_adder
	PORT MAP
	(
		X(15 DOWNTO 12), 
		Y(15 DOWNTO 12), 
		CarryOut_CarryLookAhead_3, 
		CarryOut, 
		Sum_CarryLookAhead_4
	);

		Sum(3 DOWNTO 0) <= Sum_CarryLookAhead_1;
		Sum(7 DOWNTO 4) <= Sum_CarryLookAhead_2;
		Sum(11 DOWNTO 8) <= Sum_CarryLookAhead_3;
		Sum(15 DOWNTO 12) <= Sum_CarryLookAhead_4;

END carry_lookahead_adder_arch;