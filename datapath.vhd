-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 64-Bit"
-- VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"
-- CREATED		"Mon Feb 27 09:45:30 2017"

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
use ieee.std_logic_misc.or_reduce;

LIBRARY work;

ENTITY datapath IS 
	PORT
	(
		INC_PC :  IN  STD_LOGIC;
		Gra :  IN  STD_LOGIC;
		Grb :  IN  STD_LOGIC;
		Grc :  IN  STD_LOGIC;
		Rin :  IN  STD_LOGIC;
		Rout :  IN  STD_LOGIC;
		Cout : IN STD_LOGIC;
		BAout :  IN  STD_LOGIC;
		HI_drive :  IN  STD_LOGIC;
		LO_drive :  IN  STD_LOGIC;
		Zhigh_drive :  IN  STD_LOGIC;
		Zlow_drive :  IN  STD_LOGIC;
		PC_drive :  IN  STD_LOGIC;
		MDR_drive :  IN  STD_LOGIC;
		HI_en_inpin :  IN  STD_LOGIC;
		LO_en_inpin :  IN  STD_LOGIC;
		PC_en_inpin :  IN  STD_LOGIC;
		IR_en_inpin :  IN  STD_LOGIC;
		Zhigh_en_inpin :  IN  STD_LOGIC;
		Zlow_en_inpin :  IN  STD_LOGIC;
		Y_en_inpin :  IN  STD_LOGIC;
		MDR_en_inpin :  IN  STD_LOGIC;
		OutPort_en : IN STD_LOGIC;
		InPort_drive : IN STD_LOGIC;
		read_mem :  IN  STD_LOGIC;
		write_mem : IN  STD_LOGIC;
		MAR_en :  IN  STD_LOGIC;
		CON_en :  IN  STD_LOGIC;
		ADD_OP :  IN  STD_LOGIC;
		SUB_OP :  IN  STD_LOGIC;
		MUL_OP :  IN  STD_LOGIC;
		DIV_OP :  IN  STD_LOGIC;
		AND_OP :  IN  STD_LOGIC;
		OR_OP  :  IN  STD_LOGIC;
		SHR_OP :  IN  STD_LOGIC;
		SHL_OP :  IN  STD_LOGIC;
		ROR_OP :  IN  STD_LOGIC;
		ROL_OP :  IN  STD_LOGIC;
		NEG_OP :  IN  STD_LOGIC;
		NOT_OP :  IN  STD_LOGIC;
		Clear  :  IN  STD_LOGIC;
		Clock  :  IN  STD_LOGIC;
		IO_input : IN STD_LOGIC_VECTOR(31 downto 0);
		IO_output : OUT STD_LOGIC_VECTOR(31 downto 0);
		CON_FF : OUT STD_LOGIC
	);
END datapath;

ARCHITECTURE bdf_type OF datapath IS

COMPONENT mux16to1
    PORT 
    (
        IN0, IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12, IN13, IN14, IN15  : IN std_logic_vector(31 DOWNTO 0);
        SEL       : IN std_logic_vector(3 DOWNTO 0);
        MUX_OUT   : OUT std_logic_vector (31 DOWNTO 0)
    );
END COMPONENT;

COMPONENT enc16to4
    PORT 
    (
        IN0, IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12,
		  IN13, IN14, IN15	: IN std_logic;
        ENC_OUT				: OUT std_logic_vector(3 DOWNTO 0)
    );
END COMPONENT;

COMPONENT addsub
	PORT
	(
		add_sub		: IN STD_LOGIC;
		dataa		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT enc32to5
	PORT(IN0 : IN STD_LOGIC;
		 IN1 : IN STD_LOGIC;
		 IN2 : IN STD_LOGIC;
		 IN3 : IN STD_LOGIC;
		 IN4 : IN STD_LOGIC;
		 IN5 : IN STD_LOGIC;
		 IN6 : IN STD_LOGIC;
		 IN7 : IN STD_LOGIC;
		 IN8 : IN STD_LOGIC;
		 IN9 : IN STD_LOGIC;
		 IN10 : IN STD_LOGIC;
		 IN11 : IN STD_LOGIC;
		 IN12 : IN STD_LOGIC;
		 IN13 : IN STD_LOGIC;
		 IN14 : IN STD_LOGIC;
		 IN15 : IN STD_LOGIC;
		 IN16 : IN STD_LOGIC;
		 IN17 : IN STD_LOGIC;
		 IN18 : IN STD_LOGIC;
		 IN19 : IN STD_LOGIC;
		 IN20 : IN STD_LOGIC;
		 IN21 : IN STD_LOGIC;
		 IN22 : IN STD_LOGIC;
		 IN23 : IN STD_LOGIC;
		 IN24 : IN STD_LOGIC;
		 IN25 : IN STD_LOGIC;
		 IN26 : IN STD_LOGIC;
		 IN27 : IN STD_LOGIC;
		 IN28 : IN STD_LOGIC;
		 IN29 : IN STD_LOGIC;
		 IN30 : IN STD_LOGIC;
		 IN31 : IN STD_LOGIC;
		 ENC_OUT : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
	);
END COMPONENT;

COMPONENT constant4
	PORT(		 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT constant0
	PORT(		 result : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg32
	PORT(clr : IN STD_LOGIC;
		 le : IN STD_LOGIC;
		 clk : IN STD_LOGIC;
		 reg_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 reg_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT mux2to1
	PORT(SEL : IN STD_LOGIC;
		 IN0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 IN1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 MUX_OUT : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT mybusmux
	PORT(
		data0x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data1x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data2x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data3x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data4x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data5x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data6x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data7x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data8x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data9x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data10x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data11x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data12x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data13x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data14x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data15x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data16x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data17x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data18x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data19x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data20x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data21x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data22x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data23x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data24x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data25x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data26x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data27x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data28x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data29x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data30x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data31x		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		sel			: IN STD_LOGIC_VECTOR (4 DOWNTO 0);
		result		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT booth_multiplier
	PORT 
	(
		multiplier, multiplicand  : IN std_logic_vector(31 DOWNTO 0);
		product     : OUT std_logic_vector(63 DOWNTO 0)
	);
END COMPONENT;

COMPONENT divider
	PORT
	(
		denom		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		numer		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		quotient		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		remain		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT ram IS
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (8 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rden		: IN STD_LOGIC  := '1';
		wren		: IN STD_LOGIC ;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT reg1 IS
	PORT
	(
		reg_in       : IN std_logic;
		clr, le, clk : IN std_logic;
		reg_out      : OUT std_logic
	);
END COMPONENT;

COMPONENT R0_reg is
	PORT
	(
		BAout        : IN std_logic;
		reg_in       : IN std_logic_vector(31 DOWNTO 0);
		clr, le, clk : IN std_logic;
		reg_out      : OUT std_logic_vector(31 DOWNTO 0)
	);
END COMPONENT;

COMPONENT select_encode IS
	PORT 
	(
		IR												: IN std_logic_vector(31 DOWNTO 0);
		Gra, Grb, Grc, Rin, Rout, BAout		: IN std_logic;
		C_Sign_Ext									: OUT std_logic_vector(31 DOWNTO 0);
		R15_in, R14_in, R13_in, R12_in		: OUT std_logic;
		R11_in, R10_in, R9_in, R8_in			: OUT std_logic;
		R7_in, R6_in, R5_in, R4_in				: OUT std_logic;
		R3_in, R2_in, R1_in, R0_in				: OUT std_logic;
		R15_out, R14_out, R13_out, R12_out	: OUT std_logic;
		R11_out, R10_out, R9_out, R8_out		: OUT std_logic;
		R7_out, R6_out, R5_out, R4_out		: OUT std_logic;
		R3_out, R2_out, R1_out, R0_out		: OUT std_logic
	);
END COMPONENT;

SIGNAL	bus_sig :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	clear_sig :  STD_LOGIC;
SIGNAL	clock_sig :  STD_LOGIC;
SIGNAL	HI_en :  STD_LOGIC;
SIGNAL	HI_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	IR_en :  STD_LOGIC;
SIGNAL	IR_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	LO_en :  STD_LOGIC;
SIGNAL	LO_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	MAR_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	MDR_en :  STD_LOGIC;
SIGNAL	MDR_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	PC_en :  STD_LOGIC;
SIGNAL	PC_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R0_in :  STD_LOGIC;
SIGNAL   R0_out : STD_LOGIC;
SIGNAL   R0_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R10_in :  STD_LOGIC;
SIGNAL   R10_out : STD_LOGIC;
SIGNAL   R10_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R11_in :  STD_LOGIC;
SIGNAL   R11_out : STD_LOGIC;
SIGNAL   R11_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R12_in :  STD_LOGIC;
SIGNAL   R12_out : STD_LOGIC;
SIGNAL   R12_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R13_in :  STD_LOGIC;
SIGNAL   R13_out : STD_LOGIC;
SIGNAL   R13_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R14_in :  STD_LOGIC;
SIGNAL   R14_out : STD_LOGIC;
SIGNAL   R14_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R15_in :  STD_LOGIC;
SIGNAL   R15_out : STD_LOGIC;
SIGNAL   R15_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R1_in :  STD_LOGIC;
SIGNAL   R1_out : STD_LOGIC;
SIGNAL   R1_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R2_in :  STD_LOGIC;
SIGNAL   R2_out : STD_LOGIC;
SIGNAL   R2_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R3_in :  STD_LOGIC;
SIGNAL   R3_out : STD_LOGIC;
SIGNAL   R3_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R4_in :  STD_LOGIC;
SIGNAL   R4_out : STD_LOGIC;
SIGNAL   R4_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R5_in :  STD_LOGIC;
SIGNAL   R5_out : STD_LOGIC;
SIGNAL   R5_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R6_in :  STD_LOGIC;
SIGNAL   R6_out : STD_LOGIC;
SIGNAL   R6_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R7_in :  STD_LOGIC;
SIGNAL   R7_out : STD_LOGIC;
SIGNAL   R7_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R8_in :  STD_LOGIC;
SIGNAL   R8_out : STD_LOGIC;
SIGNAL   R8_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   R9_in :  STD_LOGIC;
SIGNAL   R9_out : STD_LOGIC;
SIGNAL   R9_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	Y_en :  STD_LOGIC;
SIGNAL	Zhigh_en :  STD_LOGIC;
SIGNAL	Zhigh_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	Zlow_en :  STD_LOGIC;
SIGNAL	Zlow_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	inc_mux_output_signal :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	Y_output :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	constant_value_four :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	mdmux_output_signal :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	bus_encoder_signal :  STD_LOGIC_VECTOR(4 DOWNTO 0);
SIGNAL	constant_zero_signal :  STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_encoder_output : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL	alu_add_sub_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_mul_output : STD_LOGIC_VECTOR(63 DOWNTO 0);
SIGNAL	alu_div_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_and_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_or_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_shr_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_shl_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_ror_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_rol_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_neg_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_not_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_muldiv_mux_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_div_remainder_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	alu_mux_output : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL	constant_value_zero_32bit : STD_LOGIC_VECTOR(31 DOWNTO 0) := x"00000000";
SIGNAL	IR : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL   con_ff_input : STD_LOGIC;
SIGNAL   InPort_output : STD_LOGIC_VECTOR(31 downto 0);
signal ram_output : std_logic_vector(31 downto 0);
signal C_Sign_Ext : std_LOGIC_VECTOR(31 downto 0);

BEGIN

b2v_addition : addsub
PORT MAP(
		 add_sub => ADD_OP,
		 dataa => inc_mux_output_signal,
		 datab => bus_sig,
		 result => alu_add_sub_output);

b2v_bus_encoder : enc32to5
PORT MAP(IN0 => R0_out,
		 IN1 => R1_out,
		 IN2 => R2_out,
		 IN3 => R3_out,
		 IN4 => R4_out,
		 IN5 => R5_out,
		 IN6 => R6_out,
		 IN7 => R7_out,
		 IN8 => R8_out,
		 IN9 => R9_out,
		 IN10 => R10_out,
		 IN11 => R11_out,
		 IN12 => R12_out,
		 IN13 => R13_out,
		 IN14 => R14_out,
		 IN15 => R15_out,
		 IN16 => HI_drive,
		 IN17 => LO_drive,
		 IN18 => Zhigh_drive,
		 IN19 => Zlow_drive,
		 IN20 => PC_drive,
		 IN21 => MDR_drive,
		 IN22 => InPort_drive,
		 IN23 => Cout,
		 IN24 => '0',
		 IN25 => '0',
		 IN26 => '0',
		 IN27 => '0',
		 IN28 => '0',
		 IN29 => '0',
		 IN30 => '0',
		 IN31 => '0',
		 ENC_OUT => bus_encoder_signal);

b2v_my_bus_mux : mybusmux
PORT MAP(data0x => R0_output,
		 data1x => R1_output,
		 data2x => R2_output,
		 data3x => R3_output,
		 data4x => R4_output,
		 data5x => R5_output,
		 data6x => R6_output,
		 data7x => R7_output,
		 data8x => R8_output,
		 data9x => R9_output,
		 data10x => R10_output,
		 data11x => R11_output,
		 data12x => R12_output,
		 data13x => R13_output,
		 data14x => R14_output,
		 data15x => R15_output,
		 data16x => HI_output,
		 data17x => LO_output,
		 data18x => Zhigh_output,
		 data19x => Zlow_output, 
		 data20x => PC_output,
		 data21x => MDR_output,
		 data22x => InPort_output,
		 data23x => C_Sign_Ext,
		 data24x => MDR_output, -- NOT USED
		 data25x => MDR_output, -- NOT USED
		 data26x => MDR_output, -- NOT USED
		 data27x => MDR_output, -- NOT USED
		 data28x => MDR_output, -- NOT USED
		 data29x => MDR_output, -- NOT USED
		 data30x => MDR_output, -- NOT USED
		 data31x => MDR_output, -- NOT USED
		 sel => bus_encoder_signal,
		 result => bus_sig);

alu_muldiv_mux : mux2to1
PORT MAP(IN0 => alu_div_remainder_output,
	IN1 => alu_mul_output(63 DOWNTO 32),
	SEL => MUL_OP,
	MUX_OUT => alu_muldiv_mux_output);

b2v_constant_value_4 : constant4
PORT MAP(		 result => constant_value_four);

b2v_constant_zero : constant0
PORT MAP(		 result => constant_zero_signal);

b2v_HI_reg : reg32
PORT MAP(clr => clear_sig,
		 le => HI_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => HI_output);

b2v_INCmux : mux2to1
PORT MAP(SEL => INC_PC,
		 IN0 => Y_output,
		 IN1 => constant_value_four,
		 MUX_OUT => inc_mux_output_signal);

b2v_IR_reg : reg32
PORT MAP(clr => clear_sig,
		 le => IR_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => IR_output);

b2v_LO_reg : reg32
PORT MAP(clr => clear_sig,
		 le => LO_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => LO_output);

b2v_PC_reg : reg32
PORT MAP(clr => clear_sig,
		 le => PC_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => PC_output);

b2v_R0_reg : R0_reg
PORT MAP(clr => clear_sig,
		 le => R0_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R0_output,
		 BAout => BAout);

b2v_R10_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R10_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R10_output);

b2v_R11_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R11_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R11_output);

b2v_R12_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R12_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R12_output);

b2v_R13_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R13_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R13_output);


b2v_R14_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R14_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R14_output);

b2v_R15_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R15_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R15_output);

b2v_R1_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R1_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R1_output);

b2v_R2_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R2_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R2_output);

b2v_R3_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R3_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R3_output);

b2v_R4_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R4_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R4_output);

b2v_R5_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R5_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R5_output);

b2v_R6_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R6_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R6_output);

b2v_R7_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R7_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R7_output);

b2v_R8_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R8_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R8_output);

b2v_R9_reg : reg32
PORT MAP(clr => clear_sig,
		 le => R9_in,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => R9_output);

b2v_ALU_MUX : mux16to1
PORT MAP
(
   IN0 => alu_add_sub_output,
	IN1 => alu_add_sub_output,
	IN2 => alu_mul_output(31 DOWNTO 0),
	IN3 => alu_div_output,
	IN4 => alu_and_output,
	IN5 => alu_or_output,
	IN6 => alu_shr_output,
	IN7 => alu_shl_output,
	IN8 => alu_ror_output,
	IN9 => alu_rol_output,
	IN10 => alu_neg_output,
	IN11 => alu_not_output,
	IN12 => constant_zero_signal,
	IN13 => constant_zero_signal,
	IN14 => constant_zero_signal,
	IN15 => constant_zero_signal,
   SEL => alu_encoder_output,
   MUX_OUT => alu_mux_output);

b2v_ALU_ENC : enc16to4
PORT MAP
(
	IN0 => ADD_OP,
	IN1 => SUB_OP,
	IN2 => MUL_OP,
	IN3 => DIV_OP,
	IN4 => AND_OP,
	IN5 => OR_OP,
	IN6 => SHR_OP,
	IN7 => SHL_OP,
	IN8 => ROR_OP,
	IN9 => ROL_OP,
	IN10 => NEG_OP,
	IN11 => NOT_OP,
	IN12 => constant_zero_signal(31),
	IN13 => constant_zero_signal(31),
	IN14 => constant_zero_signal(31),
	IN15 => constant_zero_signal(31),
	ENC_OUT => alu_encoder_output);
	
big_bad_booth : booth_multiplier
PORT MAP
(
	multiplier => inc_mux_output_signal,
	multiplicand => bus_sig,
	product => alu_mul_output
);

b2v_Y_reg : reg32
PORT MAP(clr => clear_sig,
		 le => Y_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => Y_output);

b2v_Zhigh_reg : reg32
PORT MAP(clr => clear_sig,
		 le => Zhigh_en,
		 clk => clock_sig,
		 reg_in => alu_muldiv_mux_output,
		 reg_out => Zhigh_output);

b2v_Zlow_reg : reg32
PORT MAP(clr => clear_sig,
		 le => Zlow_en,
		 clk => clock_sig,
		 reg_in => alu_mux_output,
		 reg_out => Zlow_output);
		 
alu_divider : divider
PORT MAP
(
	denom => bus_sig,
	numer => inc_mux_output_signal,
	quotient => alu_div_output,
	remain => alu_div_remainder_output
);

-- Memory subsystem
MAR_reg : reg32
PORT MAP(clr => clear_sig,
		 le => MAR_en,
		 clk => clock_sig,
		 reg_in => bus_sig,
		 reg_out => MAR_output);

MDMux_mux : mux2to1
PORT MAP(SEL => read_mem,
		 IN0 => bus_sig,
		 IN1 => ram_output,
		 MUX_OUT => mdmux_output_signal);

MDR_reg : reg32
PORT MAP(clr => clear_sig,
		 le => MDR_en,
		 clk => clock_sig,
		 reg_in => mdmux_output_signal,
		 reg_out => MDR_output);

MY_RAM : ram
PORT MAP
(
	address => MAR_output(8 DOWNTO 0),
	clock	=> clock_sig,
	data => MDR_output,
	rden => read_mem,
	wren => write_mem,
	q => ram_output
);

-- CON FF Logic
CON_FF_dff : reg1
PORT MAP
(
	reg_in => con_ff_input,
	clr => clear_sig,
	le => CON_en,
	clk => clock_sig,
	reg_out => CON_FF
);

con_ff_input <=   (not or_reduce(bus_sig)) WHEN (IR_output(1 downto 0) = "00") ELSE
						or_reduce(bus_sig) WHEN (IR_output(1 downto 0) = "01") ELSE
						(not bus_sig(31)) WHEN (IR_output(1 downto 0) = "10") ELSE
						bus_sig(31) WHEN (IR_output(1 downto 0) = "11");

-- I/O
InPort : reg32
port map
(clr => clear_sig,
	le => '1',
	clk => clock_sig,
	reg_in => IO_input,
	reg_out => InPort_output);

OutPort : reg32
port map
(clr => clear_sig,
	le => OutPort_en,
	clk => clock_sig,
	reg_in => bus_sig,
	reg_out => IO_output);
	
-- Select and Encode
SELENC : select_encode
PORT MAP 
(
	IR				=> IR_output,
	Gra			=> Gra,
	Grb			=> Grb,
	Grc			=> Grc,
	Rin			=> Rin,
	Rout			=> Rout,
	BAout			=> BAout,
	C_Sign_Ext	=> C_Sign_Ext,
	R15_in      => R15_in,
   R14_in      => R14_in,
   R13_in      => R13_in,
   R12_in      => R12_in,
   R11_in      => R11_in,
   R10_in      => R10_in,
   R9_in       => R9_in,
   R8_in       => R8_in,
   R7_in       => R7_in,
   R6_in       => R6_in,
   R5_in       => R5_in,
   R4_in       => R4_in,
   R3_in       => R3_in,
   R2_in       => R2_in,
   R1_in       => R1_in,
   R0_in       => R0_in,
   R15_out     => R15_out,
   R14_out     => R14_out,
   R13_out     => R13_out,
   R12_out     => R12_out,
   R11_out     => R11_out,
   R10_out     => R10_out,
   R9_out      => R9_out,
   R8_out      => R8_out,
   R7_out      => R7_out,
   R6_out      => R6_out,
   R5_out      => R5_out,
   R4_out      => R4_out,
   R3_out      => R3_out,
   R2_out      => R2_out,
   R1_out      => R1_out,
   R0_out      => R0_out);

alu_and_output <= inc_mux_output_signal and bus_sig;
alu_or_output <= inc_mux_output_signal or bus_sig;
alu_shl_output <= std_logic_vector(shift_left(unsigned(inc_mux_output_signal), to_integer(unsigned(bus_sig))));
alu_shr_output <= std_logic_vector(shift_right(unsigned(inc_mux_output_signal), to_integer(unsigned(bus_sig))));
alu_not_output <= not inc_mux_output_signal;
alu_neg_output <= std_logic_vector(-signed(inc_mux_output_signal));
alu_rol_output <= std_logic_vector(ROTATE_LEFT(unsigned(inc_mux_output_signal), to_integer(unsigned(bus_sig))));
alu_ror_output <= std_logic_vector(ROTATE_RIGHT(unsigned(inc_mux_output_signal), to_integer(unsigned(bus_sig))));
clear_sig <= Clear;
clock_sig <= Clock;
HI_en <= HI_en_inpin;
LO_en <= LO_en_inpin;
Zhigh_en <= Zhigh_en_inpin;
Zlow_en <= Zlow_en_inpin;
Y_en <= Y_en_inpin;
PC_en <= PC_en_inpin;
MDR_en <= MDR_en_inpin;
IR <= IR_output;
IR_en <= IR_en_inpin;

END bdf_type;